/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidades.Alimento;
import facade.AlimentoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class AlimentoControle implements Serializable{
    
    private Alimento alimento;
    @Inject
    private AlimentoFacade alimentoFacade;

    
    public void novo(){
         alimento = new Alimento();
    }
    
    public String salvar(){
        try {
            alimentoFacade.salvar(alimento);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Alimento ali){
        alimentoFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Alimento> getListagem(){
        return alimentoFacade.listar();
    }

    public Alimento getAlimento() {
        return alimento;
    }

    public void setAlimento(Alimento alimento) {
        this.alimento = alimento;
    }        
}

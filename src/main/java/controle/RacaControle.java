/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidades.Raca;
import facade.RacaFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class RacaControle implements Serializable{
    
    private Raca Raca;
    @Inject
    private RacaFacade RacaFacade;
    
    public void novo(){
        Raca = new Raca();
    }
    
    public String salvar(){
        try {
            RacaFacade.salvar(Raca);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Raca raca){
        RacaFacade.excluir(raca);
        return "list?faces-redirect=true";
    }
    
    public List<Raca> getListagem(){
        return RacaFacade.listar();
    }

    public Raca getRaca() {
        return Raca;
    }

    public void setRaca(Raca Raca) {
        this.Raca = Raca;
    }        
}

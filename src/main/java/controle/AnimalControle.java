/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import converter.ConverterGenerico;
import entidades.Animal;
import facade.AnimalFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class AnimalControle implements Serializable{
    
    private Animal animal;
    @Inject
    private AnimalFacade animalFacade;
    private ConverterGenerico converterGenerico;
    
    
    public ConverterGenerico converter() {
        if (converterGenerico == null) {
            converterGenerico = new ConverterGenerico(animalFacade);
        }
        return converterGenerico;
    }
    
    public void novo(){
         animal = new Animal();
    }
    
    public String salvar(){
        try {
            animalFacade.salvar(animal);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Animal ali){
        animalFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Animal> getListagem(){
        return animalFacade.listar();
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import converter.ConverterGenerico;
import entidades.Lote;
import entidades.TransferLote;
import facade.LoteFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class LoteControle implements Serializable{
    
    private Lote Lote;
    @Inject
    private LoteFacade LoteFacade;
    private ConverterGenerico converterGenerico;
    
    
    
    public ConverterGenerico converter() {
        if (converterGenerico == null) {
            converterGenerico = new ConverterGenerico(LoteFacade);
        }
        return converterGenerico;
    }
    
    public void novo(){
        Lote = new Lote();
    }
    
    public String salvar(){
        try {
            LoteFacade.salvar(Lote);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Lote ali){
        LoteFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Lote> getListagem(){
        return LoteFacade.listar();
    }

    public Lote getLote() {
        return Lote;
    }

    public void setLote(Lote Lote) {
        this.Lote = Lote;
    }        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidades.Servico;
import facade.ServicoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class ServicoControle implements Serializable{
    
    private Servico servico;
    @Inject
    private ServicoFacade servicoFacade;
    
    public void novo(){
        servico = new Servico();
    }
    
    public String salvar(){
        try {
            servicoFacade.salvar(servico);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Servico ali){
        servicoFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Servico> getListagem(){
        return servicoFacade.listar();
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidades.Perda;
import facade.PerdaFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class PerdaControle implements Serializable{
    
    private Perda Perda;
    @Inject
    private PerdaFacade PerdaFacade;
    
    public void novo(){
        Perda = new Perda();
    }
    
    public String salvar(){
        try {
            PerdaFacade.salvar(Perda);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Perda ali){
        PerdaFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Perda> getListagem(){
        return PerdaFacade.listar();
    }

    public Perda getPerda() {
        return Perda;
    }

    public void setPerda(Perda Perda) {
        this.Perda = Perda;
    }        
}

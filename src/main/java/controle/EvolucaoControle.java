/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import entidades.Evolucao;
import facade.EvolucaoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Pazini
 */
@Named
@ViewScoped
public class EvolucaoControle implements Serializable{
    
    private Evolucao Evolucao;
    @Inject
    private EvolucaoFacade EvolucaoFacade;
    
    public void novo(){
        Evolucao = new Evolucao();
    }
    
    public String salvar(){
        try {
            EvolucaoFacade.salvar(Evolucao);
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return null;
    }
    
    public String excluir(Evolucao ali){
        EvolucaoFacade.excluir(ali);
        return "list?faces-redirect=true";
    }
    
    public List<Evolucao> getListagem(){
        return EvolucaoFacade.listar();
    }

    public Evolucao getEvolucao() {
        return Evolucao;
    }

    public void setEvolucao(Evolucao Evolucao) {
        this.Evolucao = Evolucao;
    }        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author Pazini
 */
public enum UnidadeMedida {
    
    ARROBA("Arroba"),
    QUILO("Quilo");
    
    private final String descricao;
    
    private UnidadeMedida(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}

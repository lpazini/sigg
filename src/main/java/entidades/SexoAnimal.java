/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author Pazini
 */
public enum SexoAnimal {
    
    MACHO("Macho"),
    FEMEA("Femea");
    
    private final String descricao;
    
    private SexoAnimal(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Animal;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.Transacional;

/**
 *
 * @author Pazini
 */
@Transacional
public class AnimalFacade extends AbstractFacade<Animal> implements Serializable {

    @Inject
    private EntityManager em;

    public AnimalFacade() {
        super(Animal.class);
    }

    @Override
    protected EntityManager getEm() {
        return em;
    }

    public List<Animal> animalAutoComplete(String nome) {
        Query q = em.createQuery("FROM Animal AS a WHERE LOWER(a.nome) LIKE('%" + nome.toLowerCase() + "%')");
        return q.getResultList();
    }
}

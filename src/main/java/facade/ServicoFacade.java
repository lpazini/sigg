/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Servico;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.Transacional;

/**
 *
 * @author Pazini
 */
@Transacional
public class ServicoFacade extends AbstractFacade<Servico> implements Serializable {

    @Inject
    private EntityManager em;

    public ServicoFacade() {
        super(Servico.class);
    }

    @Override
    protected EntityManager getEm() {
        return em;
    }

    public List<Servico> servicoAutoComplete(String nome) {
        Query q = em.createQuery("FROM Servico AS a WHERE LOWER(a.nome) LIKE('%" + nome.toLowerCase() + "%')");
        return q.getResultList();
    }
}

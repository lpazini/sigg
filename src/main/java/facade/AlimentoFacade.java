/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Alimento;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.Transacional;

/**
 *
 * @author Pazini
 */
@Transacional
public class AlimentoFacade extends AbstractFacade<Alimento> implements Serializable {

    @Inject
    private EntityManager em;

    public AlimentoFacade() {
        super(Alimento.class);
    }

    @Override
    protected EntityManager getEm() {
        return em;
    }

    public List<Alimento> alimentoAutoComplete(String nome) {
        Query q = em.createQuery("FROM Alimento AS a WHERE LOWER(a.nome) LIKE('%" + nome.toLowerCase() + "%')");
        return q.getResultList();
    }
}

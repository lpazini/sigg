/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Evolucao;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.Transacional;

/**
 *
 * @author Pazini
 */
@Transacional
public class EvolucaoFacade extends AbstractFacade<Evolucao> implements Serializable {

    @Inject
    private EntityManager em;

    public EvolucaoFacade() {
        super(Evolucao.class);
    }

    @Override
    protected EntityManager getEm() {
        return em;
    }

    public List<Evolucao> evolucaoAutoComplete(String nome) {
        Query q = em.createQuery("FROM Evolucao AS a WHERE LOWER(a.nome) LIKE('%" + nome.toLowerCase() + "%')");
        return q.getResultList();
    }
}

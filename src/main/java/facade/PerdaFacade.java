/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Perda;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.Transacional;

/**
 *
 * @author Pazini
 */
@Transacional
public class PerdaFacade extends AbstractFacade<Perda> implements Serializable {

    @Inject
    private EntityManager em;

    public PerdaFacade() {
        super(Perda.class);
    }

    @Override
    protected EntityManager getEm() {
        return em;
    }

    public List<Perda> perdaAutoComplete(String nome) {
        Query q = em.createQuery("FROM Perda AS a WHERE LOWER(a.nome) LIKE('%" + nome.toLowerCase() + "%')");
        return q.getResultList();
    }
}
